import { StyleSheet, View,Text} from 'react-native'
import React from 'react'
import { DrawerItem,DrawerContentScrollView } from '@react-navigation/drawer'
import {
    Avatar,
    Title,
    Caption,
    Paragraph,
    Drawer,
    TouchableRipple,
    Switch
}
from 'react-native-paper'

const DrawerContent = () => {
  return (
    <DrawerContentScrollView>
        <View style={styles.drawerContent}>
            <View style={styles.userInfoSection}>
                <Avatar.Image
                size={80}
                source={require('../../assets/profile.png')}
                />
                <Title style={styles.title}>BIR BDR TAMANG</Title>
                <Caption style={styles.caption}>@birbdrtamang</Caption>
 
                <View style={styles.row}>
                    <View style={styles.section}>
                        <Paragraph style={[styles.paragraph,styles.caption]}>54</Paragraph>
                        <Caption style={styles.caption}>Following</Caption>
                    </View>

                    <View style={styles.section}>
                        <Paragraph style={[styles.paragraph,styles.caption]}>199</Paragraph>
                        <Caption style={styles.caption}>Followers</Caption>
                    </View>
                </View>
            </View>

            <Drawer.Section style={styles.drawerSection}>
                <DrawerItem 
                    label="Setting"
                    onPress={()=>{alert("hello")}}
                />
            </Drawer.Section>

            <Drawer.Section title='Preferences'>
                <TouchableRipple onPress={()=>{}}>
                    <View style={styles.preference}>
                        <Text>Notifications</Text>
                        <View  pointerEvents='none'>
                            <Switch value={false} />
                        </View>
                    </View>
                </TouchableRipple>  
            </Drawer.Section>
        </View>
    </DrawerContentScrollView>
  )
}

export default DrawerContent

const styles = StyleSheet.create({
    drawerContent:{
        flex:1,
        margin:20
    },
    userInfoSection:{
        paddingLeft:20
    },
    title:{
        marginTop:20,
        fontWeight:'bold'
    },
    caption:{
        fontSize:14,
        lineHeight:14
    },
    row:{
        marginTop:20,
        flexDirection:'row',
        alignItems:'center',
    },
    section:{
        flexDirection:'row',
        alignItems:'center',
        marginRight:15,
    },
    paragraph:{
        fontWeight:'bold',
        marginRight:3,
    },
    drawerSection:{
        marginTop:15
    },
    preference:{
        flexDirection:'row',
        justifyContent:'space-between',
        paddingVertical:12,
        paddingHorizontal:16
    }

})