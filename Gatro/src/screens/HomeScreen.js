import { StyleSheet, Text, View,FlatList } from 'react-native'
import React from 'react'
import Background from '../component/Background'
import Header from '../component/Header'
import Button from '../component/Button'


const HomeScreen = ({navigation}) => {
  const data=[
    {
      id:1,
      name:'Bir Bdr Tamang'
    },
    {
      id:2,
      name:'Bir Bdr Tamang'
    },
    {
      id:3,
      name:'Bir Bdr Tamang'
    },
    {
      id:4,
      name:'Bir Bdr Tamang'
    },
  ]

  const displayItems=({item})=>{
    return <Text>{item.name}</Text>
  }
  return (
     <Background>
        <FlatList  
          data={data}
          renderItem={displayItems}
        />
    </Background>
  )
}

export default HomeScreen

const styles = StyleSheet.create({})