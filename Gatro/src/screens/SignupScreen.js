import { StyleSheet, Text, View,StatusBar,TouchableOpacity} from 'react-native'
import React, {useState}from 'react'
import Header from '../component/Header'
import Button from '../component/Button'
import Background from '../component/Background'
import Logo from '../component/Logo'
import TextInput from '../component/TextInput'
import { emailValidator } from '../core/helpers/emailValidator'
import { passwordValidator } from '../core/helpers/passwordValidator'
import BackButon from '../component/BackButton'
import  Paragraph  from '../component/Paragraph'
import { nameValidation } from '../core/helpers/nameValidation'
import {phoneValidator} from '../core/helpers/phoneValidator'
import { confirmPasswordValidator } from '../core/helpers/confirmPasswordValidator'
import { signUpUser } from '../api/auth-api'

const SignupScreen = ({navigation}) => {

  const [name,setName]=useState({value:'',error:''})
  const [email,setEmail]=useState({value:'',error:''}) 
  const [phone,setPhone]=useState({value:null,error:''})
  const [password,setPassword]=useState({value:'',error:''})
  const [confirmPassword,setConfirmPassword]=useState({value:'',error:''})
  const [loading,setLoading]=useState()

  const onSignupPressed=async({navigation})=>{

    const emailError=emailValidator(email.value)
    const passwordError=passwordValidator(password.value)
    const nameError=nameValidation(name.value)
    const phoneError=phoneValidator(phone.value)
    const confirmPasswordError=confirmPasswordValidator(confirmPassword.value,password.value)

  if(emailError || passwordError || nameError || phoneError || confirmPasswordError){

    setName({...name,error:nameError}) 
    setEmail({...email,error:emailError})
    setPhone({...phone,error:phoneError})
    setPassword({...password,error:passwordError})
    setConfirmPassword({...confirmPassword,error:confirmPasswordError})

    }
    setLoading(true)
    const response=await signUpUser({
      name:name.value,
      email:email.value,
      password:password.value
    })
    if(response.error){
      alert(response.error)
    }
    else{
      navigation.navigate('DrawerScreen')
    }
    setLoading(false)
    
  }

  return (
    <Background>
      <StatusBar style='auto'/>
      <BackButon goBack={navigation.goBack}/>
      <Logo/>
      <Header>Create Account</Header>
      <TextInput 
        label="Name"
        error={name.error}
        errorText={name.error}
        value={name.value}
        onChangeText={(text)=>setName({value:text,error:''})}
      />
      <TextInput 
        keyboardType='number-pad'
        maxLength={8}
        label="Email"
        value={email.value}
        error={email.error}
        errorText={email.error}
        onChangeText={(text)=>setEmail({value:text,error:""})}
      />
      <TextInput  
        label="Phone Number"
        value={phone.value}
        error={phone.error}
        errorText={phone.error}
        onChangeText={(text)=>setPhone({value:text,error:''})}
      />

      <TextInput 
        secureTextEntry 
        label="Password"
        value={password.value}
        error={password.error}
        errorText={password.error}
        onChangeText={(text)=>setPassword({value:text,error:""})}
      />

      <TextInput 
        secureTextEntry 
        label="Confirm Password"
        value={confirmPassword.value}
        error={confirmPassword.error}
        errorText={confirmPassword.error}
        onChangeText={(text)=>setConfirmPassword({value:text,error:""})}
      />

      <Button loading={loading} mode='contained' onPress={onSignupPressed} >Sign up</Button>
      <View style={styles.row}>
        <Paragraph>Already have an account? </Paragraph>
        <TouchableOpacity>
          <Text style={styles.signupText} onPress={()=>navigation.replace("LoginScreen")}>Log In</Text>
        </TouchableOpacity> 
      </View>
    </Background>
  )
}

export default SignupScreen

const styles = StyleSheet.create({
  row:{
    flexDirection:'row',
    justifyContent:'center'
  },
  signupText:{
   fontWeight:'bold'
  }
})