import { StyleSheet, Text, View,StatusBar,TouchableOpacity } from 'react-native'
import React, {useState}from 'react'
import Header from '../component/Header'
import Button from '../component/Button'
import Background from '../component/Background'
import Logo from '../component/Logo'
import TextInput from '../component/TextInput'
import { emailValidator } from '../core/helpers/emailValidator'
import { passwordValidator } from '../core/helpers/passwordValidator'
import BackButton from '../component/BackButton'
import  Paragraph  from '../component/Paragraph'
import { theme } from '../core/theme'

const LoginScreen = ({navigation}) => {
    const [email,setEmail]=useState({value:'',error:''})
    const [password,setPassword]=useState({value:'',error:''})

    const onLoginPressed=()=>{

      const emailError=emailValidator(email.value)
      const passwordError=passwordValidator(password.value)

      if(emailError || passwordError){
        setEmail({...email,error:emailError})
        setPassword({...password,error:passwordError})
      }
      else{
        navigation.navigate("DrawerScreen")
      }
      
    }
  return (
    <Background>
      <StatusBar style='auto'/>
      <BackButton goBack={navigation.goBack}/>
      <Logo/>
      <Header>Gatro</Header>
      <TextInput 
        label="Email"
        value={email.value}
        error={email.error}
        errorText={email.error}
        onChangeText={(text)=>setEmail({value:text,error:""})}
      />
      <TextInput 
        secureTextEntry 
        label="Password"
        value={password.value}
        error={password.error}
        errorText={password.error}
        onChangeText={(text)=>setPassword({value:text,error:""})}
      />
      <View style={styles.forgotPassword}>
        <TouchableOpacity onPress={()=>navigation.replace("ResetPasswordScreen")}>
          <Text style={styles.forgotText}>Forgot password?</Text>
        </TouchableOpacity>
      </View>

      <Button mode='contained' onPress={onLoginPressed} >Login</Button>
      <View style={styles.row}>
        <Paragraph>Don't have an account? </Paragraph>
        <TouchableOpacity>
          <Paragraph style={styles.signupText} onPress={()=>navigation.replace("SignupScreen")}>Sign Up</Paragraph>
        </TouchableOpacity> 
      </View>
    </Background>
  )
}

export default LoginScreen

const styles = StyleSheet.create({
  row:{
    flexDirection:'row',
    justifyContent:'center'
  },
  signupText:{
    marginTop:1,
   fontWeight:'bold',
   color:theme.colors.primary
  },
  forgotPassword:{
    alignItems:'flex-end',
    width:'100%',
    marginBottom:24
  },
  forgotText:{
    color:theme.colors.secondary
  }
})