import 'react-native-gesture-handler';
import { StyleSheet, Text, View,Image,Button } from 'react-native';
import { NavigationContainer} from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {Provider } from 'react-native-paper';
import { theme } from './src/core/theme';
import {
  StartScreen,
  LoginScreen,
  SignupScreen,
  ResetPasswordScreen,
  HomeScreen,
  ForgotPasswordOtpVarification,
  DrawerContent
} from './src/screens'; 
import firebase from 'firebase/app'
import { firebaseConfig } from './src/core/config';

import { createDrawerNavigator } from '@react-navigation/drawer';

if(!firebase.apps.length){
  firebase.initializeApp(firebaseConfig)
}

const Stack = createNativeStackNavigator();
const Drawer=createDrawerNavigator();


export default function App() {
  return (
    <Provider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator 
          initialRouteName='StartScreen'
          screenOptions={{headerShown:false}}
        >
          <Stack.Screen name='StartScreen' component={StartScreen}/>
          <Stack.Screen name='LoginScreen' component={LoginScreen}/>
          <Stack.Screen name='SignupScreen' component={SignupScreen}/>
          <Stack.Screen name='ResetPasswordScreen' component={ResetPasswordScreen} />
          <Stack.Screen name='DrawerScreen' component={DrawerNavigation} />
          <Stack.Screen name='OtpVerification' component={ForgotPasswordOtpVarification} />
        </Stack.Navigator>

      </NavigationContainer>
    </Provider>
  );
}

function DrawerNavigation(){
  return(
    <Drawer.Navigator drawerContent={DrawerContent}>
      <Drawer.Screen name='HomeScreen' component={HomeScreen} />
    </Drawer.Navigator>
  )
}




const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
